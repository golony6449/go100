FROM python:3.7.3
MAINTAINER parkseongheum <ab6449@gmail.com>
WORKDIR /source

RUN apt-get update

COPY . ${WORKDIR}
RUN apt install -y gunicorn3
RUN apt install -y dumb-init
RUN pip3 install gunicorn
RUN pip3 install -r requirements.txt

EXPOSE 8000
#ENTRYPOINT python manage.py runserver 0.0.0.0:80
# Why Should I use gunicorn? Not gunicorn3
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/source/entrypoint.sh"]
#CMD gunicorn -b 0.0.0.0:8000 go100.wsgi:application