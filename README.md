# go100
## 개요
* 종합설계 및 프로젝트 Back-end Repository

## 팀원 (Back-end)
* 박성흠
* 이영일 (youngil028@naver.com)

## 팀원 (Front-end)
* 강현석
* 문윤기
* 김성재

## Reference
* Django - REST - PWA 예제: https://github.com/milooy/react-django-pwa-kit
* DRF: https://www.django-rest-framework.org/
* 로그인: https://velog.io/@killi8n/Dnote-5-1.-Django-%EA%B6%8C%ED%95%9C-%EC%84%A4%EC%A0%95-%EB%B0%8F-%EB%A1%9C%EA%B7%B8%EC%9D%B8-%ED%9A%8C%EC%9B%90%EA%B0%80%EC%9E%85-%EA%B5%AC%ED%98%84-tmjmep5tcm
