from django.contrib import admin

from classroom.models import *


admin.site.register(Classroom, admin.ModelAdmin)
admin.site.register(Notice, admin.ModelAdmin)
admin.site.register(Homework, admin.ModelAdmin)
admin.site.register(StudentInClass, admin.ModelAdmin)
admin.site.register(Qna, admin.ModelAdmin)
admin.site.register(Answer, admin.ModelAdmin)

# # Register your models here.
# admin.site.register(get_user_model(), UserAdmin)
