# Generated by Django 2.2 on 2019-05-23 23:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0003_classroom_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='homework',
            name='subject_name',
            field=models.CharField(default=False, max_length=50),
            preserve_default=False,
        ),
    ]
