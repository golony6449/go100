# Generated by Django 2.2 on 2019-06-05 13:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0009_auto_20190604_0109'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='notice',
            name='type',
        ),
    ]
