# Generated by Django 2.2 on 2019-06-06 20:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('classroom', '0012_answer'),
    ]

    operations = [
        migrations.RenameField(
            model_name='answer',
            old_name='Answer',
            new_name='answer',
        ),
    ]
