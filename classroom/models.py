from django.db import models
from django.contrib.auth.models import AbstractUser, User


class Classroom(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    teacher = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField(blank=True, default="")


class StudentInClass(models.Model):
    classroom = models.ForeignKey('Classroom', on_delete=models.CASCADE)
    student = models.ForeignKey(User, on_delete=models.CASCADE)


class Notice(models.Model):
    classroom = models.ForeignKey('Classroom', on_delete=models.CASCADE)
    id = models.AutoField(primary_key=True)
    # TODO: null=True 제거
    registrant = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=100)
    content = models.TextField()
    register_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)


class Homework(Notice):
    end = models.DateTimeField()


class Qna(models.Model):
    classroom = models.ForeignKey('Classroom', on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    is_Answerd = models.BooleanField(default=False)          # 답변 여부
    post_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)  # 게시물 날짜
    Q_content = models.TextField()              # 질문 내용
    reg_date = models.DateTimeField(auto_now_add=True)

class Answer(models.Model):
    qna = models.ForeignKey('Qna', on_delete=models.CASCADE)
    answer = models.TextField()
    writer = models.ForeignKey(User, on_delete=models.CASCADE)


# # Reference: https://wikidocs.net/10294
# class User(AbstractUser):
#     is_teacher = models.BooleanField(default=False, null=False)
