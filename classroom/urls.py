from django.contrib import admin
from django.urls import path

from classroom.views import *

urlpatterns = [
    path('list', ClassListView.as_view(), name='classlist'),
    path('noticepost', NoticeView.as_view(), name='noticepost'),
    path('allnotice', GetAllNoticeView.as_view(), name='getAllNotice'),
    path('homework', HomeWork.as_view(), name='homework'),
    path('getqna', GetAQna.as_view(), name='getqna'),
    path('getallqna', GetAllQna.as_view(), name='getallqna'),
    path('newclass', CreateClassroom.as_view(), name='newclass'),
    path('apply', ApplyStudent.as_view(), name='apply'),
    path('getstudentlist', getStudentList.as_view(), name='getstudentlist'),
    path('deletestudent', DeleteStudent.as_view(), name='deletestudent'),
    path('writeqna', WriteQna.as_view(), name='writeqna'),
    path('deleteclass', deleteClass.as_view(), name='deleteclass'),
    path('writehomework', WriteHomework.as_view(), name='writehomework'),
    path('setdescription', description.as_view(), name='desc'),
    path('writenotice', WriteNotice.as_view(), name='writenotice'),
    path('deleteclass', deleteClass.as_view(), name='deleteclass'),
    path('getclassinfo', GetClassInfo.as_view(), name='getclassinfo'),
    path('answer', CreateNewAnswer.as_view(), name='answer')
]
