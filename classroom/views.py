from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model, authenticate, login, logout

from classroom.models import *
import go100.module.token as token
from custom_auth.models import *

from go100.module.firebase import Firebase

import json


def index(request):
    return render(request, 'index.html')


@method_decorator(csrf_exempt, name='dispatch')
class ApplyStudent(View):
    def get(self, request):
        return HttpResponse('ApplyStudent: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        class_key = Classroom.objects.get(id=request_data['class_id'])
        user_key = User.objects.get(username=request_data['user_id'])

        if token.validate(request_data['user_token'], response_data, classroom=class_key) is None:
            return JsonResponse(response_data)

        try:
            new_student = StudentInClass(classroom=class_key, student=user_key)
            new_student.save()
            response_data['status'] = True
        except ValueError:
            response_data['status'] = False

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class getStudentList(View):
    def get(self, request):
        return HttpResponse('Get Student List: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        classroom = Classroom.objects.get(id=request_data['class_id'])
        students = StudentInClass.objects.filter(classroom=classroom)

        if token.validate(request_data['user_token'], response_data, classroom=classroom) is None:
            return JsonResponse(response_data)

        response_data['list'] = list()
        for std in students:
            student_dict = dict()
            #학생 구별자가 ID이므로 idx는 불필요
            student_dict['user_id'] = std.student.username
            student_dict['name'] = std.student.last_name + std.student.first_name
            response_data['list'].append(student_dict)
        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class DeleteStudent(View):
    def get(self, request):
        return HttpResponse('DeleteStudent: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        #인덱스가 아닌 아이디를 받아서 지우자
        classroom = Classroom.objects.get(id=request_data['class_id'])
        student = User.objects.get(username=request_data['user_id'])

        if token.validate(request_data['user_token'], response_data, classroom=classroom) is None:
            return JsonResponse(response_data)

        try:
            delete_stu = StudentInClass.objects.get(classroom=classroom, student=student)
            delete_stu.delete()
            response_data['status'] = True

        except StudentInClass.DoesNotExist:
            response_data['status'] = False

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class WriteQna(View):
    def get(self, request):
        return HttpResponse('WriteQna: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        user_key = User.objects.get(username=request_data['user_id'])
        classroom = Classroom.objects.get(id=request_data['class_id'])

        if token.validate(request_data['user_token'], response_data, classroom=classroom) is None:
            return JsonResponse(response_data)

        try:
            new_qna = Qna(classroom=classroom, title=request_data['title'],
                          user=user_key, Q_content=request_data['content'])
            new_qna.save()
            response_data['status'] = True
        except ValueError:
            response_data['status'] = False
        except Qna.DoesNotExist:
            response_data['status'] = False

        push = Firebase()

        fcm_list = Token.objects.filter(user=user_key)

        for fcm_token in fcm_list:
            if fcm_token.fcm_token != "":
                try:
                    push.send_notification(fcm_token.fcm_token, "QnA 등록", "새로운 QnA가 등록되었습니다.")
                except:
                    print(fcm_token.user.username, '에게 전송중 에러발생')

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class GetAllQna(View):
    def get(self, request):
        return HttpResponse('GetAllQna: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response = dict()

        classroom = Classroom.objects.get(id=int(request_data['class_id']))
        print('success')
        qna_list = Qna.objects.filter(classroom=classroom).order_by('-reg_date')

        if token.validate(request_data['user_token'], response, classroom=classroom) is None:
            return JsonResponse(response)

        response['list'] = list()

        for qna in qna_list:
            temp_val = dict()
            temp_val['post_id'] = qna.post_id
            temp_val['isAnswered'] = qna.is_Answerd
            temp_val['title'] = qna.title
            temp_val['reg_date'] = qna.reg_date.strftime('%Y-%m-%d %H:%M:%S')

            response['list'].append(temp_val)

        response['status'] = True
        return JsonResponse(response)


@method_decorator(csrf_exempt, name='dispatch')
class GetAQna(View):
    def get(self, request):
        return HttpResponse('GetAQna: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        class_chk = Classroom.objects.get(id=int(request_data['class_id']))
        qna = Qna.objects.get(classroom=class_chk, post_id=int(request_data['post_id']))

        if token.validate(request_data['user_token'], response_data, class_chk) is None:
            return JsonResponse(response_data)

        response_data['title'] = qna.title
        response_data['isAnswered'] = qna.is_Answerd
        response_data['writer_id'] = qna.user.username
        response_data['date'] = qna.date.strftime('%Y-%m-%d %H:%M:%S')
        response_data['q_contents'] = qna.Q_content

        answer_list = Answer.objects.filter(qna=qna)
        response_data['a_contents'] = list()

        for answer in answer_list:
            temp = dict()
            temp['user_id'] = answer.writer.username
            temp['contents'] = answer.answer
            response_data['a_contents'].append(temp)


        response_data['status'] = True

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class ClassListView(View):
    def get(self, request):
        return HttpResponse('ClassListView: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response = dict()

        if token.validate(request_data['user_token'], response) is None:
            return JsonResponse(response)

        token_obj = Token.objects.get(user_token=request_data['user_token'])
        response['Context'] = list()

        # 강사의 경우
        if token_obj.user.additionaluserinfo.is_teacher:
            class_list = Classroom.objects.filter(teacher=token_obj.user)

            for cls in class_list:
                each_cls = dict()
                each_cls['class_id'] = cls.id
                each_cls['class_name'] = cls.name
                each_cls['class_desc'] = cls.description
                each_cls['class_teacher'] = cls.teacher.username
                each_cls['is_own'] = True

                response['Context'].append(each_cls)

        # 수강 중인 수업
        class_list = StudentInClass.objects.filter(student=token_obj.user)

        for cls in class_list:
            each_cls = dict()
            each_cls['class_id'] = cls.classroom.id
            each_cls['class_name'] = cls.classroom.name
            each_cls['class_desc'] = cls.classroom.description
            each_cls['class_teacher'] = cls.classroom.teacher.username
            each_cls['is_own'] = False

            response['Context'].append(each_cls)

        response['status'] = True

        return JsonResponse(response)


@method_decorator(csrf_exempt, name='dispatch')
class NoticeView(View):
    def get(self, request):
        return HttpResponse('NoticeView: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        classroom = Classroom.objects.get(id=int(request_data['class']))
        notice = Notice.objects.get(classroom=classroom, id=int(request_data['idx']))

        if token.validate(request_data['user_token'], response_data, classroom=classroom) is None:
            return JsonResponse(response_data)

        response_data['userID'] = notice.registrant.username
        response_data['reg_date'] = notice.register_date.strftime('%Y-%m-%d %H:%M:%S')
        response_data['update_date'] = notice.update_date.strftime('%Y-%m-%d %H:%M:%S')
        response_data['title'] = notice.title
        response_data['content'] = notice.content

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class GetAllNoticeView(View):
    def get(self, request):
        return HttpResponse('GetAllNoticeView: Invaild Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        # 정보 추출
        classroom = Classroom.objects.get(id=int(request_data['class']))
        all = Notice.objects.all()
        notice_list = Notice.objects.filter(classroom=classroom).order_by('-register_date')

        # 토큰 검증 및 사용자 정보 전달
        if token.validate(request_data['user_token'], response_data, classroom=classroom) is None:
            return JsonResponse({'status': False, 'msg': 'Invalid user_token'})

        response_data['list'] = list()

        for idx, obj in enumerate(notice_list):
            obj_dict = dict()
            obj_dict['noticeid'] = obj.id
            obj_dict['username'] = obj.registrant.username
            obj_dict['reg_date'] = obj.register_date.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict['update_date'] = obj.update_date.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict['title'] = obj.title

            response_data['list'].append(obj_dict)

        response_data['status'] = True
        response_data['msg'] = 'DO Successfully'

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class HomeWork(View):
    def get(self, request):
        return HttpResponse('HomeWork : Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        classroom = Classroom.objects.get(id=int(request_data['class']))
        homework_list = Homework.objects.filter(classroom=classroom).order_by('-register_date')

        if token.validate(request_data['user_token'], response_data, classroom=classroom) is None:
            return JsonResponse(response_data)

        response_data['list'] = list()
        for obj in homework_list:
            obj_dict = dict()
            obj_dict['title'] = obj.title
            obj_dict['content'] = obj.content
            obj_dict['DeadLine'] = obj.end.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict['reg_date'] = obj.register_date.strftime('%Y-%m-%d %H:%M:%S')

            response_data['list'].append(obj_dict)

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class CreateClassroom(View):
    def get(self, request):
        return HttpResponse('CreateClassroom: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response = dict()

        if token.validate(request_data['user_token'], response) is None:
            return JsonResponse(response)

        if response['is_teacher'] is False:
            response['status'] = False
            response['msg'] = 'Permission Denied: User is not teacher'
            return JsonResponse(response)

        teacher_token = Token.objects.get(user_token=request_data['user_token'])

        new_class = Classroom(name=request_data['name'], description=request_data['desc'],
                              teacher=teacher_token.user)
        new_class.save()

        response['status'] = True
        return JsonResponse(response)


@method_decorator(csrf_exempt, name='dispatch')
class deleteClass(View):
    def get(self, request):
        return HttpResponse('DeleteClass: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        classroom = Classroom.objects.get(id=request_data['class_id'])

        if token.validate(request_data['user_token'], response_data, classroom=classroom) is None:
            return JsonResponse(response_data)

        classroom.delete()
        response_data['status'] = True

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class WriteHomework(View):
    def get(self, request):
        return HttpResponse('WriteHomework: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        classroom = Classroom.objects.get(id=request_data['class_id'])
        user_key = Token.objects.get(user_token=request_data['user_token'])

        if token.validate(request_data['user_token'], response_data, classroom=classroom) is None:
            return JsonResponse(response_data)

        import datetime
        # TODO 수정: end
        new_homework = Homework(classroom=classroom, registrant=user_key.user,
                                title=request_data['title'], content=request_data['content'],
                                end=datetime.datetime.strptime(request_data['deadline'], '%Y-%m-%d %H:%M'))
        new_homework.save()
        response_data['isSuccess'] = True

        push = Firebase()
        stu_list = StudentInClass.objects.filter(classroom=classroom)

        for stu in stu_list:
            fcm_list = Token.objects.filter(user=stu.student)

            for fcm_token in fcm_list:
                if fcm_token.fcm_token != "":
                    try:
                        push.send_notification(fcm_token.fcm_token, "QnA 등록", "새로운 QnA가 등록되었습니다.")
                    except:
                        print(fcm_token.user.username, '에게 전송중 에러발생')

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class WriteNotice(View):
    def get(self, request):
        return HttpResponse('WriteNotice: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        classroom = Classroom.objects.get(id=request_data['class_id'])
        user_key = Token.objects.get(user_token=request_data['user_token'])

        if token.validate(request_data['user_token'], response_data, classroom=classroom) is None:
            return JsonResponse(response_data)

        new_notice = Notice(classroom=classroom, title=request_data['title'],
                            registrant=user_key.user, content=request_data['content'])

        new_notice.save()
        response_data['isSuccess'] = True

        push = Firebase()
        stu_list = StudentInClass.objects.filter(classroom=classroom)

        for stu in stu_list:
            fcm_list = Token.objects.filter(user=stu.student)

            for fcm_token in fcm_list:
                if fcm_token.fcm_token != "":
                    try:
                        push.send_notification(fcm_token.fcm_token, "QnA 등록", "새로운 QnA가 등록되었습니다.")
                    except:
                        print(fcm_token.user.username, '에게 전송중 에러발생')

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class description(View):
    def get(self, request):
        return HttpResponse('description: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        classroom = Classroom.objects.get(id=request_data['class_id'])

        if token.validate(request_data['user_token'], response_data, classroom=classroom) is None:
            return JsonResponse(response_data)

        classroom.description = request_data['about']
        classroom.save()
        response_data['isSuccess'] = True

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class GetClassInfo(View):
    def get(self, request):
        return HttpResponse("GetClassInfo: Invalid Approach")

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        classroom = Classroom.objects.get(id=request_data['class_id'])

        if token.validate(request_data['user_token'], response_data, classroom=classroom) is None:
            return JsonResponse(response_data)

        response_data['class_name'] = classroom.name
        response_data['about'] = classroom.description
        response_data['isSuccess'] = True

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class CreateNewAnswer(View):
    def get(self, request):
        return HttpResponse('CreateNewAnswer: Invlaid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        if token.validate(request_data['user_token'], response_data) is None:
            return JsonResponse(response_data)

        try:
            qna = Qna.objects.get(post_id=request_data['post_id'])
            qna.is_Answerd = True
            qna.save()

            token_obj = Token.objects.get(user_token=request_data['user_token'])
            new_answer = Answer(qna=qna, answer=request_data['a_contents'],
                                writer=token_obj.user)
            new_answer.save()
            response_data['isSuccess'] = True
        except Qna.DoesNotExist:
            response_data['isSuccess'] = False

        push = Firebase()

        fcm_list = Token.objects.filter(user=qna.user)

        for fcm_token in fcm_list:
            if fcm_token.fcm_token != "":
                try:
                    push.send_notification(fcm_token.fcm_token, "QnA 등록", "새로운 QnA가 등록되었습니다.")
                except:
                    print(fcm_token.user.username, '에게 전송중 에러발생')

        return JsonResponse(response_data)