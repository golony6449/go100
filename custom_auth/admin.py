from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from custom_auth.models import *

# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class UserAdditionInfoInline(admin.StackedInline):
    model = AdditionalUserInfo
    can_delete = False
    verbose_name_plural = 'Addtional user infos'


class UserAuthInfoInline(admin.StackedInline):
    model = Token
    can_delete = False
    verbose_name_plural = 'auth informations'

# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (UserAdditionInfoInline, UserAuthInfoInline, )


class AddtionalUserInfoAdmin(admin.ModelAdmin):
    fields = ['user', 'is_teacher']


class TokenAdmin(admin.ModelAdmin):
    fields = ['user', 'user_token', 'fcm_token']

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(AdditionalUserInfo, AddtionalUserInfoAdmin)
admin.site.register(Token, TokenAdmin)
