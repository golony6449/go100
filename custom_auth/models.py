from django.db import models
from django.contrib.auth.models import User


class AdditionalUserInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_teacher = models.BooleanField(default=False)


class Token(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_token = models.CharField(max_length=100)
    fcm_token = models.TextField(blank=True, default="")
