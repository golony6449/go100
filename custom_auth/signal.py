# 유저 확장을 위한 post_save 구현
# https://dgkim5360.tistory.com/entry/django-signal-example
# https://cjh5414.github.io/extending-user-model-using-one-to-one-link/

from django.db.models.signals import post_save
from django.dispatch import receiver

from custom_auth.models import AdditionalUserInfo
from django.contrib.auth.models import User


@receiver(post_save, sender=User)
def additional_user_create_job(sender, **kwargs):
    if kwargs['created'] is True:
        AdditionalUserInfo.objects.create(user=kwargs['instance'])


@receiver(post_save, sender=User)
def additional_user_job(sender, **kwargs):
    kwargs['instance'].additionaluserinfo.save()
