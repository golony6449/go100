from django.contrib import admin
from django.urls import path

from custom_auth.views import *

urlpatterns = [
    path('firebase-test', Firebase_testView.as_view(), name='firebase-test'),
    path('login', LoginView.as_view(), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('join', JoinView.as_view(), name='join'),
    path('add_token', AddTokenView.as_view(), name='add_token'),
    path('delete_token', DeleteTokenView.as_view(), name='delete_token'),
    path('find_student', FindStudentsView.as_view(), name='find_student'),
    path('createuser', CreateNewStudent.as_view(), name='createuser'),
]
