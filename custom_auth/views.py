from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model, authenticate, login, logout

from custom_auth.models import *
from go100.module.token import validate

import json


@method_decorator(csrf_exempt, name='dispatch')
class LoginView(View):
    def get(self, request):
        return HttpResponse('LoginView: Invalid Approach')

    def post(self, request):
        request_values = json.loads(request.body)
        print('id', request_values['id'], '\t password: ', request_values['password'])

        response = dict()

        user = authenticate(request, username=request_values['id'], password=request_values['password'])
        if user is None:
            response['status'] = False
            response['msg'] = 'Login Failure: Incorrect Username or PW'

        else:
            login(request, user)
            new_token = Token(user=user, user_token=self.make_token())
            new_token.save()

            # TODO: 세션 만료시, 토큰 삭제
            response['status'] = True
            response['msg'] = 'Login Success'
            response['id'] = user.username
            response['user_token'] = new_token.user_token

        return JsonResponse(response)

    def make_token(self, length=30):
        from random import randint
        source = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKMNOPQRSTUVWXYZ'
        result = ''

        for i in range(length):
            result = result + source[randint(0, len(source) - 1)]

        return result


@method_decorator(csrf_exempt, name='dispatch')
class LogoutView(View):
    def get(self, request):
        return HttpResponse('LogoutView: Invalid Approach')

    def post(self, request):
        try:
            logout(request)
        except:
            print("ERROR: couldn't logout")

        request_data = json.loads(request.body)
        response = dict()
        token_list = Token.objects.all()

        for token in token_list:
            if token.user_token == request_data['user_token']:
                response['status'] = True
                response['msg'] = "Logout and revoke token successfully."
                response['id'] = token.user.username
                token.delete()
                break
        else:
            response['status'] = False
            response['msg'] = "Invalid Token. Please Check again"

        return JsonResponse(response)


class JoinView(View):
    def get(self, request):
        pass

    def post(self, request):
        pass


@method_decorator(csrf_exempt, name='dispatch')
class AddTokenView(View):
    def get(self, request):
        return HttpResponse('AddTokenView: Invalid Approach')

    def post(self, request):
        response = dict()
        request_values = json.loads(request.body)

        if validate(request_values['user_token'], response):
            JsonResponse(response)

        token_list = Token.objects.all()

        for token in token_list:
            if token.user_token == request_values['user_token']:
                token.fcm_token = request_values['fcm_token']
                token.save()

                response['status'] = True
                response['msg'] = 'FCM_TOKEN add successfully'
                response['ID'] = token.user.username

                status_code = 200
                break

        else:
            response['status'] = False
            response['msg'] = "Wrong user_token. Please Check again"
            status_code = 400

        return JsonResponse(response, status=status_code)


@method_decorator(csrf_exempt, name='dispatch')
class DeleteTokenView(View):
    def get(self, request):
        return HttpResponse("DeleteTokenView: Invalid Approach")

    def post(self, request):
        request_data = json.loads(request.body)
        response = dict()

        if validate(request_data['user_token'], response) is None:
            return JsonResponse(response)

        token_list = Token.objects.all()

        for token in token_list:
            if token.fcm_token == request_data['fcm_token']:
                token.fcm_token = ""
                token.save()

                response['status'] = True
                response['msg'] = 'Delete Token Successfully'
                status_code = 200
                break

        else:
            response['status'] = False
            response['msg'] = 'Invalid FCM Token'
            status_code = 400

        return JsonResponse(response, status=status_code)


class Firebase_testView(View):
    def get(self, request):
        return render(request, 'firebase-test.html')

    def post(self, request):
        from go100.module.firebase import Firebase
        fireobj = Firebase()
        fireobj.send_notification(title=request.POST['title'], body=request.POST['content'],
                                  token=request.POST['token'])

        return redirect('/firebase-test')


@method_decorator(csrf_exempt, name='dispatch')
class FindStudentsView(View):
    def get(self, request):
        return HttpResponse('FindStudentsView: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response = dict()

        if validate(request_data['user_token'], response) is None:
            return JsonResponse(response)

        response['list'] = list()
        user_list = User.objects.filter(username__contains=request_data['stu_id'])

        for user in user_list:
            temp = dict()
            temp['user_id'] = user.username
            temp['user_name'] = user.last_name + user.first_name
            response['list'].append(temp)

        response['status'] = True
        return JsonResponse(response)


@method_decorator(csrf_exempt, name='dispatch')
class CreateNewStudent(View):
    def get(self, request):
        return HttpResponse('CreateNewStudent: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response = dict()

        if validate(request_data['user_token'], response) is None:
            return JsonResponse(response)

        new_student = User.objects.create_user(username=request_data['user_name'],
                                               first_name=request_data['first_name'],
                                               last_name=request_data['last_name'],
                                               password=request_data['password'])
        new_student.save()

        response['isSuccess'] = True
        return JsonResponse(response)
