#!/bin/bash

python3 manage.py collectstatic --noinput
mkdir /source/log

gunicorn -b 0.0.0.0:8000 --access-logfile /source/log/access.log --error-logfile /source/log/error.log go100.wsgi:application