#!/bin/bash

mkdir /source/log

daphne -b 0.0.0.0 -p 8001 --access-log /source/log/ws_access.log go100.asgi:application