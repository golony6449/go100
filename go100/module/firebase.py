import firebase_admin
from firebase_admin import credentials, messaging

import logging
from django.conf import settings

class Firebase:
    def __init__(self):
        self.cred = credentials.Certificate(settings.CREDENTIAL_PATH)
        try:
            self.default_app = firebase_admin.initialize_app(self.cred)
        except ValueError:
            print('이미 app이 존재함')
        self.url = 'https://fcm.googleapis.com/fcm/send'


    def send_notification(self, token, title, body):
        notification = messaging.Notification(title=title, body=body)
        message = messaging.Message(notification=notification, token=token)
        response = messaging.send(message)
        logging.info(response)
        print('send successfully')
