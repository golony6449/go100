from custom_auth.models import *


def validate(user_token, result, classroom=None):
    token_obj = Token.objects.get(user_token=user_token)

    # 유효한 토큰 없음
    if token_obj is None:
        result['status'] = False
        result['msg'] = 'Invalid user token'
        return None

    result['id'] = token_obj.user.username

    if classroom is None:
        result['is_teacher'] = token_obj.user.additionaluserinfo.is_teacher

    elif classroom.teacher == token_obj.user:
        result['is_teacher'] = True

    else:
        result['is_teacher'] = False

    return True
