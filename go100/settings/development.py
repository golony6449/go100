from go100.settings.base import *
from json import load

# SECURITY WARNING: keep the secret keys.json used in production secret!
try:
    with open('./go100/keys.json') as input:
        key_secret = load(input)
    SECRET_KEY = key_secret['SECRET_KEY']
    FCM_SERVER_KEY = key_secret['FCM_SERVER_KEY']
    CREDENTIAL_PATH = key_secret['CREDENTIAL_PATH']
except FileNotFoundError:
    SECRET_KEY = os.environ['SECRET_KEY']


ALLOWED_HOSTS = ['127.0.0.1']

try:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': key_secret['DB_NAME'],
            'USER': key_secret['DB_ADMIN'],
            'PASSWORD': key_secret['DB_PASSWORD'],
            'HOST': key_secret['DB_HOST_ADDR'],
            'PORT': key_secret['DB_PORT']
        }
    }
except KeyError:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, '../../db.sqlite3'),
        }
    }

# django-channel
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
    },
}