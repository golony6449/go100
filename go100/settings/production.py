from go100.settings.base import *

SECRET_KEY = os.environ['SECRET_KEY']

ALLOWED_HOSTS = ['golony.dev', '127.0.0.1']
SECRET_KEY = os.environ['SECRET_KEY']
CREDENTIAL_PATH = os.environ['CREDENTIAL_PATH']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ['DB_NAME'],
        'USER': os.environ['DB_ADMIN'],
        'PASSWORD': os.environ['DB_PASSWORD'],
        'HOST': os.environ['DB_HOST_ADDR'],
        'PORT': os.environ['DB_PORT']
    }
}

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('redis', 6379)],
        },
    },
}