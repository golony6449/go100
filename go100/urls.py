"""go100 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from classroom.views import index

urlpatterns = [
    path('index.html',
         TemplateView.as_view(template_name='index.html'), name='index.html'),
    path('service-worker.js',
         TemplateView.as_view(template_name='service-worker.js', content_type='application/javascript'), name='service-worker.js'),
    path('manifest.json',
         TemplateView.as_view(template_name='manifest.json', content_type='application/json'), name='manifest.json'),
    path('precache-manifest.18fee5b65eb31779407f5e294625e4c8.js',
         TemplateView.as_view(template_name='precache-manifest.18fee5b65eb31779407f5e294625e4c8.js', content_type='application/javascript')),
    path('firebase-messaging-sw.js',
         TemplateView.as_view(template_name='firebase-messaging-sw.js', content_type='application/javascript')),

    path('admin/', admin.site.urls),
    # path('api-auth/', include('rest_framework.urls')),
    path('api/auth/', include('custom_auth.urls')),
    path('api/exam/', include('problem.urls')),
    path('api/classroom/', include('classroom.urls')),
    path('api/livequiz/', include('livequiz.urls')),
    path('api/problem/', include('problem.urls')),
    path('', index, name='index'),
]
