from django.contrib import admin

from livequiz.models import *

admin.site.register(LiveQuizSet, admin.ModelAdmin)
admin.site.register(LiveQuiz, admin.ModelAdmin)
admin.site.register(LiveQuizChoice, admin.ModelAdmin)
admin.site.register(Client, admin.ModelAdmin)
