from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json

from classroom.models import *
from custom_auth.models import *
from livequiz.models import *


class LiveQuizConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # 테스트 코드
        print('scope', self.scope)
        print('channel_name', self.channel_name, 'type', type(self.channel_name))
        async_to_sync(print('channel_layer', self.channel_layer))

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        Client.objects.filter(channel_name=self.channel_name).delete()

        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {'type': 'update_count'}
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        received = json.loads(text_data)

        try:
            method = received['method']
        except KeyError:
            # TODO: Legacy
            message = received['message']
            # Send message to room group
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {'type': 'chat_message', 'message': message}
            )
            return

        if method == 'c2s_init':
            self.initialize(received)

        elif method == 'c2s_start':
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name, {'type': 'start', 'json_data': received}
            )
        elif method == 'c2s_answer':
            self.answer(received)

        elif method == 'c2s_next':
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name, {'type': 'next', 'json_data': received}
            )
        elif method == 'c2s_result':
            self.result(received)

    # TODO: Legacy
    # Receive message from room group
    def chat_message(self, event):
        message = event['message']

        obj_list = LiveQuizSet.objects.all()

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': len(obj_list)
        }))

    def initialize(self, event):
        print('init procedure start')

        room_number = int(self.scope['url_route']['kwargs']['room_name'])
        user_obj = Token.objects.get(user_token=event['user_token'])
        user_id = user_obj.user.username

        Client.objects.create(
            room_number=room_number,
            user_token=event['user_token'],
            user_id=user_id,
            channel_name=self.channel_name,
            is_teacher=user_obj.user.additionaluserinfo.is_teacher
        )

        quiz_set = LiveQuizSet.objects.get(id=room_number)
        quiz_set.is_active = True
        quiz_set.save()

        response = dict()
        response['is_teacher'] = user_obj.user.additionaluserinfo.is_teacher
        response['method'] = 's2c_init'

        async_to_sync(self.channel_layer.send)(self.channel_name, {
            "type": "response_unicast",
            "response": response,
        })

        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {'type': 'update_count'}
        )

    def start(self, event):
        print('start procedure start')
        response = dict()
        response['method'] = 's2c_start'

        quiz_set = LiveQuizSet.objects.get(id=int(self.scope['url_route']['kwargs']['room_name']))

        quiz_list = LiveQuiz.objects.filter(quiz_set=quiz_set)
        quiz = LiveQuiz.objects.get(quiz_set=quiz_set, sequence=int(event['json_data']['reqIdx']))

        response['count'] = len(quiz_list)
        response['reqIdx'] = quiz.sequence

        response['choiceable'] = quiz.choiceable
        response['context'] = quiz.context
        response['answer'] = quiz.answer
        response['example'] = list()

        try:
            response['img'] = quiz.image.url
        except ValueError:
            pass

        for choice in LiveQuizChoice.objects.filter(quiz=quiz):
            temp = dict()
            temp['code'] = choice.number
            temp['context'] = choice.text
            response['example'].append(temp)

        self.send(text_data=json.dumps(response))

    def answer(self, event):
        print('answer procedure start')

        quiz_set = LiveQuizSet.objects.get(id=int(self.scope['url_route']['kwargs']['room_name']))

        quiz = LiveQuiz.objects.get(quiz_set=quiz_set, sequence=int(event['reqIdx']))

        if quiz.answer == event['answer']:
            client = Client.objects.get(user_token=event['user_token'])
            client.score = client.score + 1
            client.save()

        else:
            pass

    def next(self, event):
        print('next procedure start')

        quiz_set = LiveQuizSet.objects.get(id=int(self.scope['url_route']['kwargs']['room_name']))
        quiz = LiveQuiz.objects.get(quiz_set=quiz_set, sequence=int(event['json_data']['reqIdx']))

        response = dict()
        response['answer'] = quiz.answer
        response['method'] = 's2c_next'

        self.send(text_data=json.dumps(response))

    def result(self, event):
        print('result procedure start')

        response = dict()
        response['list'] = list()

        room_number = int(self.scope['url_route']['kwargs']['room_name'])
        quiz_set = LiveQuizSet.objects.get(id=room_number)
        quiz_set.is_active = False
        quiz_set.save()

        for client in Client.objects.all():
            if client.is_teacher is True:
                response['method'] = 's2c_report'
                response['list'].clear()

                for student in Client.objects.filter(is_teacher=False).order_by('-score'):
                    temp = dict()
                    temp['user_id'] = student.user_id
                    temp['score'] = student.score

                    response['list'].append(temp)

                async_to_sync(self.channel_layer.send)(client.channel_name, {
                    "type": "response_unicast",
                    "response": response,
                })

            else:
                response['method'] = 's2c_result'
                response['list'].clear()

                response['list'].append({
                    'user_id': client.user_id,
                    'score': client.score,
                })

                async_to_sync(self.channel_layer.send)(client.channel_name, {
                    "type": "response_unicast",
                    "response": response,
                })

    def response_unicast(self, event):
        self.send(text_data=json.dumps(event['response']))

    def update_count(self, event):
        print('update_count procedure start')

        response = dict()

        room_number = int(self.scope['url_route']['kwargs']['room_name'])
        user_list = Client.objects.filter(room_number=room_number)

        response['user_count'] = len(user_list)
        response['method'] = 's2c_update_count'

        self.send(text_data=json.dumps(response))
