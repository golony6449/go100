from django.db import models


class LiveQuizSet(models.Model):
    id = models.AutoField(primary_key=True)
    classroom = models.ForeignKey('classroom.Classroom', on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    create_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=False)


class LiveQuiz(models.Model):
    sequence = models.IntegerField()
    quiz_set = models.ForeignKey('LiveQuizSet', on_delete=models.CASCADE)
    choiceable = models.BooleanField()
    context = models.TextField()
    image = models.ImageField(null=True)
    answer = models.CharField(max_length=100)


class LiveQuizChoice(models.Model):
    quiz = models.ForeignKey('LiveQuiz', on_delete=models.CASCADE)
    number = models.IntegerField()
    text = models.CharField(max_length=100)


class Client(models.Model):
    room_number = models.IntegerField()
    user_token = models.CharField(max_length=50)
    user_id = models.CharField(max_length=50)
    channel_name = models.CharField(max_length=100)
    score = models.IntegerField(default=0)
    is_teacher = models.BooleanField(default=False)
