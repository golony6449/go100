from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/livequiz/(?P<room_name>[^/]+)/$', consumers.LiveQuizConsumer),
]