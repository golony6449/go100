from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.http.response import HttpResponse, JsonResponse
from django.contrib.auth.models import User

import go100.module.token as token
from classroom.models import *
from custom_auth.models import *
from livequiz.models import *

import json

# Create your views here.
def index(request):
    return render(request, 'chat.html')

def room(request, room_name):
    return render(request, 'room.html',
                  {'room_name_json': mark_safe(json.dumps(room_name))})


@method_decorator(csrf_exempt, name='dispatch')
class CreateNewLiveQuiz(View):
    def get(self, request):
        return HttpResponse("CreateNewLiveQuiz: Invalid Approach")

    def post(self, request):
        request_data = json.loads(request.body)
        response = dict()

        if token.validate(request_data['user_token'], response) is None:
            return JsonResponse(response)

        if response['is_teacher'] is False:
            response['status'] = False
            response['msg'] = 'Permission Denied: User is not teacher'
            return JsonResponse(response)

        classroom = Classroom.objects.get(id=int(request_data['class']))
        new_quiz_set = LiveQuizSet(classroom=classroom, title=request_data['title'])
        new_quiz_set.save()

        for idx, quiz in enumerate(list(request_data['quizzes'])):
            new_quiz = LiveQuiz(quiz_set=new_quiz_set, choiceable=quiz['choiceable'],
                                context=quiz['context'], answer=str(quiz['answer']), sequence=idx+1)
            try:
                new_quiz.image = quiz['img']
            except KeyError:
                pass

            new_quiz.save()

            try:
                quiz['example']
            except KeyError:
                pass

            else:
                for choice in list(quiz['example']):
                    new_choice = LiveQuizChoice(quiz=new_quiz, number=int(choice['code']),
                                                text=choice['context'])
                    new_choice.save()

        response['status'] = True
        return JsonResponse(response)


@method_decorator(csrf_exempt, name='dispatch')
class GetLiveQuizListView(View):
    def get(self, request):
        return HttpResponse('GetLiveQuizList: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response = dict()

        if token.validate(request_data['user_token'], response) is None:
            return JsonResponse(response)

        classroom = Classroom.objects.get(id=request_data['class'])

        if response['is_teacher'] is True:
            quiz_list = LiveQuizSet.objects.filter(classroom=classroom)
        else:
            quiz_list = LiveQuizSet.objects.filter(classroom=classroom, is_active=True)

        response['list'] = list()

        for quizset in quiz_list:
            temp = dict()
            temp['code'] = quizset.id
            temp['title'] = quizset.title
            temp['quizzes_num'] = len(LiveQuiz.objects.filter(quiz_set=quizset))
            temp['date'] = quizset.create_date.strftime('%Y-%m-%d %H:%M:%S')
            response['list'].append(temp)

        response['status'] = True
        return JsonResponse(response)


@method_decorator(csrf_exempt, name='dispatch')
class DeleteLiveQuizSetView(View):
    def get(self, request):
        return HttpResponse("DeleteLiveQuizSetView: Invalid Approach")

    def post(self, request):
        request_data = json.loads(request.body)
        response = dict()

        if token.validate(request_data['user_token'], response) is None:
            return JsonResponse(response)

        target = LiveQuizSet.objects.get(id=request_data['code'])
        target.delete()

        response['status'] = True
        return JsonResponse(response)
