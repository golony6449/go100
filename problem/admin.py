from django.contrib import admin
from problem.models import *


admin.site.register(Question, admin.ModelAdmin)
admin.site.register(Choice, admin.ModelAdmin)
admin.site.register(WrongAnswerNote, admin.ModelAdmin)
admin.site.register(Correction, admin.ModelAdmin)
