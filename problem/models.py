from django.db import models
from django.contrib.auth.models import User


class Question(models.Model):
    subject = models.IntegerField()
    content = models.TextField()
    image = models.ImageField(null=True, blank=True)
    id = models.AutoField(primary_key=True)
    writer_id = models.ForeignKey(User, on_delete=models.CASCADE)
    choiceable = models.BooleanField()              # 객/주관식 확인 (T = 객, F = 주)
    answer = models.CharField(max_length=50)        # 객관식인 경우, 해당 num이 문자열로 저장됨
    explanation = models.TextField()


class Choice(models.Model):
    Question = models.ForeignKey('Question', on_delete=models.CASCADE)
    number = models.IntegerField(default=0)
    text = models.CharField(max_length=100)


class WrongAnswerNote(models.Model):
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    wrong = models.CharField(max_length=150)


class Correction(models.Model):
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    score = models.IntegerField(null=True)              # 강의자가 평가한 문제 점수
    correction_content = models.TextField(blank=True, default="")   # 추가 Comment
    image = models.ImageField(null=True)
