from django.contrib import admin
from django.urls import path

from problem.views import *

urlpatterns = [
    path('marking', MarkQuestion.as_view(), name='marking'),
    path('getquestion', GetQuestion.as_view(), name='getquestion'),
    path('note', GetWrongAnswerNote.as_view(), name='note'),
    path('myquestion', GetMyQuestion.as_view(), name='myquestion'),
    path('create', CreateProblem.as_view(), name='create'),
    path('writecorrection', WriteNewCorrection.as_view(), name='writecorrection')
]
