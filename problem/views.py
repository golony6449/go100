from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model, authenticate, login, logout

from classroom.models import *
from problem.models import *
from custom_auth.models import *

import json
import random
from go100.module.token import validate
from go100.module.firebase import Firebase


@method_decorator(csrf_exempt, name='dispatch')
class GetMyQuestion(View):
    def get(self, request):
        return HttpResponse('GetMyQuestion: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        if validate(request_data['user_token'], response_data) is None:
            return JsonResponse(response_data)

        user_token = Token.objects.get(user_token=request_data['user_token'])
        questions = Question.objects.filter(writer_id=user_token.user)

        response_data['questions'] = list()
        for idx in questions:
            q_dict = dict()
            q_dict['code'] = idx.id
            q_dict['choiceable'] = idx.choiceable
            q_dict['subject'] = idx.subject
            q_dict['context'] = idx.content
            try :
                q_dict['img'] = idx.image.url
            except ValueError:
                pass
            q_dict['answer'] = idx.answer
            q_dict['explanation'] = idx.explanation

            q_dict['example'] = list()
            choices = Choice.objects.filter(Question=idx)
            for choice_idx in choices:
                ex = dict()
                ex['code'] = choice_idx.number
                ex['context'] = choice_idx.text
                q_dict['example'].append(ex)

            correct = Correction.objects.filter(question=idx)
            q_dict['solutions'] = list()
            total_score = 0

            # 임시 커밋 / 첨삭의 결과에 따라 달라짐
            for cor_idx in correct:
                ex = dict()
                ex['score'] = cor_idx.score
                ex['context'] = cor_idx.correction_content
                try:
                    ex['img'] = cor_idx.image.url
                except ValueError:
                    pass
                q_dict['solutions'].append(ex)
                total_score += cor_idx.score

            try:
                q_dict['solution_avg_score'] = total_score / len(correct)
            except ZeroDivisionError:
                pass

            response_data['questions'].append(q_dict)

        response_data['status'] = True

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class CreateProblem(View):
    def get(self, request):
        return HttpResponse('CreateProblem: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        if validate(request_data['user_token'], response_data) is None:
            return JsonResponse(response_data)

        user_token = Token.objects.get(user_token=request_data['user_token'])


        try:
            new_question = Question(writer_id=user_token.user, choiceable=bool(request_data['choiceable']),
                                 subject=request_data['subject'], content=request_data['context'],
                                 image=request_data['img'], answer=request_data['answer'],
                                 explanation=request_data['explanation'])
            new_question.save()
        except KeyError:
            new_question = Question(writer_id=user_token.user, choiceable=bool(request_data['choiceable']),
                                 subject=request_data['subject'], content=request_data['context'],
                                 answer=request_data['answer'], explanation=request_data['explanation'])
            new_question.save()

        try:
            for idx in request_data['example']:
                ex_token = Choice(Question=new_question, number=int(idx["code"]), text=idx["Context"])
                ex_token.save()
        except KeyError:
            pass

        response_data['status'] = True
        response_data['code'] = new_question.id

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class WriteQuestion(View):
    def get(self, request):
        return HttpResponse('WriteQuestion: Invalid Approach')
    def get(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        if validate(request_data['user_token'], response_data) is None:
            return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class MarkQuestion(View):
    def get(self, request):
        return HttpResponse('MarkQuestion : Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        try:
            question = Question.objects.get(id=int(request_data['code']))
        except Question.DoesNotExist:
            response_data['msg'] = "error code : {}".format(request_data['code'])
            response_data['status'] = False
            return JsonResponse(response_data)

        response_data['code'] = question.id

        if validate(request_data['user_token'], response_data) is None:
            return JsonResponse(response_data)

        if int(response_data['code']) == int(request_data['code']):
            response_data['status'] = True
        else:
            response_data['status'] = False
            response_data['msg'] = "해당하는 문제가 없습니다."
            return JsonResponse(response_data)

        user_token = Token.objects.get(user_token=request_data['user_token'])

        if request_data['marking'] == question.answer:
            response_data['msg'] = "정답입니다."
        else:
            response_data['msg'] = "오답입니다."
            try:
                Wrong_TF_chk = WrongAnswerNote.objects.get(user=user_token.user, question=question)
            except WrongAnswerNote.DoesNotExist:
                WrongNote = WrongAnswerNote(user=user_token.user, question=question, wrong=request_data['marking'])
                WrongNote.save()

        response_data['explanation'] = question.explanation
        response_data['answer'] = question.answer

        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class GetQuestion(View):
    def get(self, request):
        return HttpResponse('GetQuestion: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response_data = dict()

        if validate(request_data['user_token'], response_data) is None:
            return JsonResponse(response_data)

        # 특정 과목 랜덤 문제 n개
        if int(request_data['code']) == -1:
            question_list = Question.objects.filter(subject=int(request_data['subject']))

            if len(question_list) == 0:
                response_data['num'] = 0
                response_data['status'] = False
                response_data['msg'] = "요청하신 과목에서 해당 문제 코드가 존재하지 않습니다."
                return JsonResponse(response_data)
            else:
                response_data['num'] = len(question_list)
            ran = list()

            ran_num = random.randint(0, len(question_list)-1)
            question_count = int(request_data['num']) if int(request_data['num']) < len(question_list) - 1 else len(question_list) - 1
            for i in range(question_count):
                while ran_num in ran:
                    ran_num = random.randint(0, len(question_list)-1)

                ran.append(ran_num)
            ran.sort()

            result = list()
            for idx in ran:

                result.append(question_list[idx])

            response_data['data'] = list()

            for question in question_list:
                choice_list = Choice.objects.filter(Question=question)
                # for obj in choice_list:
                obj_dict = dict()
                obj_dict['code'] = question.id
                obj_dict['ID'] = question.writer_id.username
                obj_dict['choiceable'] = question.choiceable

                obj_dict['context'] = question.content
                try:
                    obj_dict['img'] = question.image.url
                except ValueError:
                    pass

                obj_dict['example'] = list()

                for obj_ex in choice_list:
                    exam = dict()
                    exam['code'] = obj_ex.number
                    exam['context'] = obj_ex.text

                    obj_dict['example'].append(exam)

                correction_list = Correction.objects.filter(question=question)
                total_score = 0

                for correction in correction_list:
                    total_score += correction.score

                try:
                    obj_dict['solution_avg_score'] = total_score / len(correction_list)
                except ZeroDivisionError:
                    pass

                response_data['data'].append(obj_dict)

        # 과목 코드를 이용한 특정과목 1개
        else:
            try:
                question = Question.objects.get(id=int(request_data['code']))
            except Question.DoesNotExist:
                response_data['status'] = False
                response_data['msg'] = "요청하신 과목에서 해당 문제 코드가 존재하지 않습니다."
                return JsonResponse(response_data)

            question = Question.objects.get(id=int(request_data['code']))
            response_data['data'] = list()
            request_data['num'] = 1

            obj_dict = dict()
            obj_dict['code'] = question.id
            obj_dict['ID'] = question.writer_id.username
            # 단답, 객관
            obj_dict['choiceable'] = question.choiceable
            obj_dict['context'] = question.content

            try:
                obj_dict['img'] = question.image.url
            except ValueError:
                pass

            obj_dict['example'] = list()

            choice_list = Choice.objects.filter(Question=question)
            for obj in choice_list:
                exam = dict()
                exam['code'] = obj.number
                exam['Context'] = obj.text
                obj_dict['example'].append(exam)

            correction_list = Correction.objects.filter(question=question)
            total_score = 0

            for correction in correction_list:
                total_score += correction.score

            try:
                obj_dict['solution_avg_score'] = total_score / len(correction_list)
            except ZeroDivisionError:
                pass

            response_data['data'].append(obj_dict)

        response_data['status'] = True
        return JsonResponse(response_data)


@method_decorator(csrf_exempt, name='dispatch')
class GetWrongAnswerNote(View):
    def get(self, request):
        return HttpResponse('GetWrongAnswerNote: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response = dict()

        if validate(request_data['user_token'], response) is None:
            return JsonResponse(response)

        request_user = Token.objects.get(user_token=request_data['user_token'])
        WAN_list = WrongAnswerNote.objects.filter(user=request_user.user)

        response['data'] = list()
        response['num'] = len(WAN_list)

        # 해당 유저가 틀린 문제가 없는 경우 예외처리
        if len(WAN_list) == 0:
            response['status'] = False
            response['msg'] = '해당유저의 오답노트 정보가 없습니다.'
            return JsonResponse(response)

        for note in WAN_list:
            note_obj = dict()

            note_obj['code'] = note.question.id
            note_obj['ID'] = note.question.writer_id.username
            note_obj['subject'] = note.question.subject
            note_obj['choiceable'] = note.question.choiceable
            note_obj['context'] = note.question.content
            note_obj['example'] = list()
            try:
                note_obj['img'] = note.question.image.url
            except ValueError:
                pass

            if note_obj['choiceable']:
                for choice in Choice.objects.filter(Question=note.question):
                    choice_obj = dict()

                    choice_obj['code'] = choice.number
                    choice_obj['context'] = choice.text
                    note_obj['example'].append(choice_obj)

            correction_list = Correction.objects.filter(question=note.question)
            total_score = 0

            for correction in correction_list:
                total_score += correction.score

            try:
                note_obj['solution_avg_score'] = total_score / len(correction_list)
            except ZeroDivisionError:
                pass

            response['data'].append(note_obj)

        response['status'] = True
        return JsonResponse(response)


@method_decorator(csrf_exempt, name='dispatch')
class WriteNewCorrection(View):
    def get(self, request):
        return HttpResponse('WriteNewCorrection: Invalid Approach')

    def post(self, request):
        request_data = json.loads(request.body)
        response = dict()

        if validate(request_data['user_token'], response) is None:
            return JsonResponse(response)

        # 강사 자격 확인
        if response['is_teacher'] is False:
            response['status'] = False
            response['msg'] = 'Permission Denied: Not Teacher'
            return JsonResponse(response)
        try:
            target_question = Question.objects.get(id=int(request_data['code']))
        except Question.DoesNotExist:
            response['status'] = False
            response['msg'] = "Invalid Question id"
            return JsonResponse(response)

        try:
            new_correction = Correction(question=target_question, score=int(request_data['score']),
                                        correction_content=request_data['context'],
                                        img=request_data['img'])

        except KeyError:
            new_correction = Correction(question=target_question, score=int(request_data['score']),
                                        correction_content=request_data['context'])

        new_correction.save()

        response['status'] = True

        push = Firebase()

        fcm_list = Token.objects.filter(user=target_question.writer_id)

        for fcm_token in fcm_list:
            if fcm_token.fcm_token != "":
                try:
                    push.send_notification(fcm_token.fcm_token, "QnA 등록", "새로운 QnA가 등록되었습니다.")
                except:
                    print(fcm_token.user.username, '에게 전송중 에러발생')

        return JsonResponse(response)
