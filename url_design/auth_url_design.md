# Auth URL 설계

## 인증 관련
* `get` METHOD로 접근시, `Invalid Approach` 반환

### login
* url: `api/auth/login`
* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| id | ID |
| password | 비밀번호 |


* 반환 값

| 변수 명 | 내용 | 비고 |
|--------|-------|-----|
| status | 성공 여부 | |
| id | 로그인에 성공한 ID | 성공한 경우에만 포함 |
| token | 사용자 토큰 | 성공한 경우에만 포함 |
| msg | 상태 메시지 |

### logout
* url: `api/auth/logout`

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| user_token | 사용자 토큰 |


* 반환 값

| 변수 명 | 내용 | 비고 |
|--------|-------|-----|
| status | 성공시 True, 실패시 False | |
| id | 사용자 명 | 성공시에만 포함 |
| msg | 상태 메시지 | |

### add_token
* url: `api/auth/add_token`
* 역할: fcm-token` 등록

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| user_token | 사용자 토큰 |
| fcm_token | 클라이언트의 firebase-token 값 |


* 반환 값

| 변수 명 | 내용 |
|--------|-------|
| status | 성공시 True, 실패시 False |
| ID | 성공시 유저의 ID 반환 |
| is_teacher | 성공시 요청자의 강사 여부 반환 |
| msg | 상태 메시지 |


### delete_token
* url: `api/auth/delete_token`
* 역할: fcm-token` 삭제

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| user_token | 사용자 토큰 |
| fcm_token | 클라이언트의 firebase-token 값 |


* 반환 값

| 변수 명 | 내용 |
|--------|-------|
| id | 요청자 ID |
| is_teacher | 강사 여부 |
| status | 성공시 True, 실패시 False |
| msg | 상태 메시지 |

### 학생 찾기 API
* url: `api/auth/find_student`
* 역할: 등록을 위해 학생 ID를 탐색

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| user_token | 사용자 토큰 |
| stu_id | 검색할 ID 키워드 |


* 반환 값

| 변수 명 | 내용 |
|--------|-------|
| list | 학생 정보 목록 |
| id | 요청자 ID |
| is_teacher | 요청자의 강사여부 | 

* list 구성요소

| 변수 명 | 내용 |
|--------|-------|
| user_id | 학생 ID |
| user_name | 학생 이름 |


### 학생 추가 API
* url: `api/auth/createuser`
* 역할: 새로운 학생 생성

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| user_token | 요청자의 사용자 토큰 |
| user_name | 생성할 사용자의 ID |
| first_name | 생성할 사용자의 이름 |
| last_name | 생성할 사용자의 성 |
| password | 생성할 사용자의 패스워드 |


* 반환 값

| 변수 명 | 내용 |
|--------|-------|
| isSuccess | 성공여부 |
| id | 요청자 ID |
| is_teacher | 요청자의 강사여부 | 

