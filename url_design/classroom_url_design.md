# classroom URL 설계

## Classroom
### 수업 목록 요청
* url: `api/classroom/list`
* 역할: 회원이 수강 중인 수업 목록 출력

* 전달 값

| 변수 명 | 내용 |
|---------|------|
| user_token | 사용자 토큰 |

* 반환 값

| 변수 명 | 내용 |
|---------|-------|
| status | 성공 여부 |
| id | 요청자의 id |
| is_teacher | 강사 여부 |
| Context | 수업 목록(리스트) |


* Context 구성요소

| 구성요소 명 | 내용 |
|-------------|-------|
| class_id | 수업의 고유 번호 |
| class_name | 수업 명 |
| class_desc | 수업 설명 |
| class_teacher | 강사 명 |
| is_own | 해당 수업의 강사 여부 표기 |


### 해당 수업의 공지사항 리스트 요청
* url: `api/classroom/allnotice`
* 역할: 특정한 수업에서 특정한 유형의 모든 공지사항 목록 요청

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| user_token | 유저 토큰 |
| class | 반의 고유값 |
| board_type | 요청할 공지사항 유형 |


* 반환 값

| 변수 명 | 내용 | 비고 |
|--------|-------|-------|
| status | 성공여부 | T/F |
| msg | 서버 메시지 | |
| id | 사용자 ID | |
| is_teacher | 강사 여부 | T/F |
| list | 공지사항 리스트 | |

* 리스트 구성 요소

| 변수 명 | 내용 | 비고 |
|--------|-------|-------|
| noticeid | 공지사항 글 고유값 | |
| username | 유저이름 | |
| reg_data | 등록일 | 연-월-일 시:분:초 |
| update_date | 최종 수정일 | 연-월-일 시:분:초 |
| title | 공지사항 제목 |  |


### 공지사항 데이터 요청
* url: `api/classroom/noticepost`
* 역할: 특정한 공지사항에 대한 내용 전송 요청

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| class | 반의 고유값 |
| idx | 공지사항 글의 고유값 |


* 반환 값

| 변수 명 | 내용 | 비고 |
|--------|-------|-------|
| userID | 유저이름 | |
| reg_data | 등록일 | 연-월-일 시:분:초 |
| update_date | 최종 수정일 | 연-월-일 시:분:초 |
| noticeContents | 공지사항 내용 | |

### 과제 목록 요청
* url: `api/classroom/homework`
* 역할: 특정 수업에 대한 과제 목록 요청

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| class | 반의 고유값 |
| user_token | 사용자 토큰 |


* 반환 값

| 변수 명 | 내용 | 비고 |
|--------|-------|-------|
| id | 유저이름 | |
| is_teacher | 강사여부 | |
| list | 과제 목록 포함 | |

* 과제 목록

| 변수 명 | 내용 | 비고 |
|--------|-------|-------|
| title | 과제 제목 | |
| content | 과제 내용 | |
| DeadLine | 과제 기한 | |
| reg_date | 과제 등록일 | |

### Q&A 게시글 가져오기
* url: `api/classroom/getqna`
* 역할 : 특정 수업에 대한 QnA 정보 요청

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| user_token | 사용자 토큰 |
| class_id | 학급 구별자 |
| post_id | 게시글 구별자 |

* 반환 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| id | 사용자 명 | |
| is_teacher | 강사여부 | |
| status | 성공여부 | |
| title | 글 제목 |  |
| isAnswered | 답변 여부 | T/F |
| writer_id | 글 쓴 이름 |  |
| date | 글 게시 날짜 |  |
| q_contents | 물음 내용 |  |
| a_contents | 답변 내용 | list |

* 답변내용 

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| user_id | 답변자 ID | |
| contents | 답변 내용 | |


### Q&A 목록 요청
* url: `api/classroom/getallqna`
* 역할 : 특정 수업에 대한 QnA 목록 요청

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| user_token | 사용자 토큰 |
| class_id | 학급 구별자 |

* 반환 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| id | 사용자 명 | |
| is_teacher | 강사여부 | |
| status | 성공여부 | |
| list | QnA 목록 | |

* list 내부 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| post_id | QnA 고유번호 | |
| isAnswered | 답변 여부 | |
| title | QnA 제목 | |
| reg_date | 등록일자 | |


### 수업 생성 API
* url: `api/classroom/newclass`
* 역할 : 새로운 수업 생성 요청
* 주의: 강사 자격이 있는 계정만 가능

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| user_token | 사용자 토큰 |
| name | 생성할 수업의 이름 |
| desc | 생성할 수업의 한줄 설명 |

* 반환 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| id | 사용자 명 | |
| is_teacher | 강사여부 | |
| status | 성공여부 | |
| msg | 실패시 실패 사유 기술 | |

### 강사페이지 학생 등록
* url: `api/classroom/apply`
* 역할 : 학생을 학급에 등록

* 전달 값

| 변수명 | 내용 | 자료형 |
|--------|-------|-------|
| class_id | 학급 구분자 | 정수형 |
| user_id | 학생의 ID | 학생 구분자, 문자열 | 
| user_token | 유저 토큰 | 문자열 |

* 반환 값

| 변수명 | 내용 | 자료형 |
|--------|-------|-------|
| status | 등록 성공 여부 | boolean |
| is_teacher | 선생 여부 확인 | boolean |
| id | 요청자 id | string |


### 등록된 학생 목록 가져오기
* url : `api/classroom/getstudentlist`
* 역할 : 해당 학급의 학생들 목록을 불러옴

* 전달 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| class_id | 학급 구분자 |
| user_token | 유저 토큰 |

* 반환 값

| 변수명 | 내용 | 자료형 |
|--------|-------|-------|
| list | 유저 정보 | list |
| is_teacher | 선생 자격 여부 | boolean |
| status | 성공 여부 | boolean |

* list 내용

| 변수명 | 내용 | 자료형 |
|--------|-------|-------|
| user_id | 학생의 ID | string |
| name | 학생의 이름 | string |


* 변경 사항

| 변수명 | 변경 | 사유 |
|--------|-------|-------|
| isTeacher | 삭제 | 모든 뷰에서 token 받아서 teacher 여부 확인 함 |
| student_idx | 삭제 | 유저의 id가 구별자 역할을 함 | 


### 학생 정보 삭제
* url: `api/classroom/deletestudent`
* 역할: 학급에서 학생 정보 삭제

* 전달 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| class_id | 학급 구별자 | int |
| user_id | 학생 ID | 구별자 | string |
| user_token | 토큰 | string |

* 반환 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| status | 삭제 확인 | 성공 true, 실패 false |
| is_teacher | 강사 여부 확인 |
| id | 요청자 ID | |

* 변경 사항

`Student_idx` -> `user_id`

`User`의 고유값으로 `ID`를 사용


### Q&A 글 작성하기
* url: `api/classroom/writeqna`
* 역할: Q&A 글 작성

* 전달 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| class_id | 학급 구별자 |
| user_id | 유저 id |
| title | 글 제목 |
| content | 물음 내용 |
| user_token | 유저 토큰 |

* 반환 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| is_teacher | 선생 여부 확인 |
| status | 글 작성 성공 여부 | boolean |
| id | 요청자 ID |


### 학급 삭제하기
* url: `api/classroom/deleteclass`
* 역할: 학급 삭제

* 전달 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| class_id | 학급 구분자 |
| user_token | 유저 토큰 |

* 반환 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| status | 삭제 여부 파악 | 성공 : T, 실패 : F |
| is_teacher | 선생 자격 확인 |
| id | 요청한 사용자 ID |

### 과제 작성하기
* url: `api/classroom/writehomework`
* 역할: 학급삭제

* 전달 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| class_id | 학급 구별자 |
| user_token | 사용자 토큰 |
| title | 과제 제목 |
| content | 과제 내용 |
| deadline | 과제 기한 |

* 반환 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| is_teacher | 선생 자격 확인 |
| user_id | 유저의 id |
| isSuccess | 성공 여부 |

### 학급 소개글 수정
* url: `api/classroom/setdescription`
* 역할 : 학급 소개글 수정

* 전달 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| user_token | 사용자 토큰 |
| class_id | 학급 구별자 |
| about | 학급 소개글 |

* 반환 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| isSuccess | 적용 성공 여부 |
| is_teacher | 선생 여부 확인 |
| id | 요청자 id |

### 공지사항 작성
* url: `api/classroom/writenotice`
* 역할 : 공지사항 작성

* 전달 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| user_token | 유저 토큰 |
| class_id | 학급 구분자 |
| contents | 공지 내용 |
| title | 공지사항 제목 |

* 반환 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| isSuccess | 성공 여부 |
| id | 요청자 ID | |
| msg | 실패시 사유 | |
| is_teacher | 강사 여부 | |

### 학급 정보 가져오기
* url: `api/classroom/getclassinfo`
* 역할: 학급 명, 유저 이름 가져오기

* 전달 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| user_token | 유저 토큰 |
| class_id | 학급 구별자 |

* 반환 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| is_teacher | 선생 자격 확인 |
| id | 유저 ID |
| class_name | 학급 이름 |
| about | 학급 설명 |
| isSuccess | 성공 여부 |

### Q&A 답변 적기
* url: `api/classroom/answer`
* 역할 : qna 물음에 대한 답변을 DB에 저장

* 전달 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| user_token | 유저 토큰 |
| post_id | qna 구별자 |
| a_contents | 답변 내용 |

* 반환 값

| 변수명 | 내용 | 비고 |
|--------|-------|-------|
| isSuccess | 성공 여부 |
| is_teacher | 선생 여부 |
| id | 유저 id |