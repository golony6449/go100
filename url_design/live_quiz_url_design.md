# Live Quiz URL 설계

## 문제 생성
* url: `/api/livequiz/newLiveQ`
* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| user_token | 사용자 token |
| class | 각 반의 고유번호 |
| title | 라이브 문제 Set의 제목 |
| quizzes | 문제 목록 |

* 문제 목록 값

| 변수 명 | 내용 |
|--------|-------|
| choiceable | 객관식 여부 |
| context | 문제 내용 |
| answer | 문제 정답 |
| img | 이미지 (옵션) |
| example | 보기 목록 (주관식의 경우는 전달하지 않아도 무관) |

* 문제 보기 목록

| 변수 명 | 내용 |
|--------|-------|
| code | 보기번호 |
| context | 보기 내용 |

* 반환 값

| 변수 명 | 내용 | 비고 |
|--------|-------|-----|
| id | 사용자 ID | |
| is_teacher | 강사여부 | |
| status | 성공 여부 | |
| msg | 실패시, 실패사유 | |

## LiveQuiz 목록 요청 API
* url: `/api/livequiz/livequizList`

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| user_token | 사용자 token |
| class | 리스트를 얻어올 반의 고유번호 |

* 반환 값

| 변수 명 | 내용 | 자료형 |
|--------|-------|-------|
| status | 성공여부 | boolean |
| id | 요청자의 id | string |
| is_teacher | 요청자의 강사 여부 | boolean |
| list | 라이브 퀴즈 목록 | list |

* list 내부 값

| 변수 명 | 내용 | 자료형 |
|--------|-------|-------|
| code | 고유번호 | int |
| title | 라이브 퀴즈 제목 | string |
| quizzes_num | 문제 수 | int |
| date | 생성일 | string |

## LiveQuiz 삭제 요청 API
* url: `/api/livequiz/deletelivequiz`

* 전달 값

| 변수 명 | 내용 |
|--------|-------|
| user_token | 사용자 token |
| code | Livequiz의 고유번호 |

* 반환값

| 변수 명 | 내용 | 자료형 |
|--------|-------|-------|
| status | 성공여부 | boolean |
| id | 요청자의 id | string |
| is_teacher | 요청자의 강사 여부 | boolean |